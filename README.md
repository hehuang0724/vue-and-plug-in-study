# A project to learn vue and another plug-in components


## 文件目录结构

```text
├── public                     # 静态资源
│   │── favicon.ico            # favicon图标
│   └── index.html             # html模板
├── src                        # 源代码
│   ├── assets                 # 静态资源
│   │   └── logo.png           # logo图标
│   ├── components             # 全局公用组件
│   │   └── HelloWorld         # 
│   ├── router                 # 路由
│   ├── store                  # 全局状态store管理
│   ├── utils                  # 全局公用工具类方法
│   │   ├── utils.js           # 全局工具类
│   │   └── ......             # 其他公共组件
│   ├── views                  # views 所有页面
│   ├── App.vue                # 入口页面
│   ├── main.js                # 入口文件 加载组件 初始化等
├── vue.config.js              # vue-cli 配置
├── README.md                  # 说明文档文件
└── package.json               # package.json
```

