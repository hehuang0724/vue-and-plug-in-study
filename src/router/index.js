import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/about",
    name: "About",
    component: () => import("../views/About.vue")
  },
  // slot-demo页面
  {
    path: "/slot",
    name: "SlotDemo",
    component: () => import("../views/slot-demo/SlotParent.vue")
  },
  // vue-ls页面
  {
    path: "/vuels",
    name: "VueLs",
    component: () => import("../views/vue-ls/VueLs.vue")
  },
  //vuex页面
  {
    path: "/vuex",
    name: "TestVuex",
    component: () => import("../views/vuex/TestVuex.vue")
  },
  // 无限滚动页面
  {
    path: "/infiniteScroll",
    name: "InfiniteScroll",
    component: () =>
      import("../views/vue-infinite-scroll/VueInfinitineScroll.vue")
  },
  {
    path: "/bar",
    name: "Bar",
    component: () => import("../views/echarts/DrawBar.vue")
  },
  {
    path: "/wheater",
    name: "wheater",
    component: () => import("../views/echarts/Wheater.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
