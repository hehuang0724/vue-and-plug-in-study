import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import storage from "vue-ls"; //使用vue-ls
import Antd from "ant-design-vue"; //使用Ant Design Ui框架
import "ant-design-vue/dist/antd.css";
import axios from "axios";
// 引入无限加载
import infiniteScroll from "vue-infinite-scroll";

Vue.use(infiniteScroll);

Vue.config.productionTip = false;

// vue-ls 的使用
var options = {
  // namespace: "vuels__", //key键前缀(可有可无)
  name: "ls", // 命名Vue变量.[ls]或this.[$ls],
  storage: "local" // 存储名称: session, local, memory
};
Vue.use(storage, options);
//Ant Design 的使用
Vue.use(Antd);
//
Vue.prototype.$axios = axios;

new Vue({
  router,
  store,
  render: h => h(App),
  mounted() {
    Vue.ls.set("foo", "boo");
    // 设置有效期
    Vue.ls.set("foo", "boo", 60 * 60 * 1000); //有效1小时
    Vue.ls.get("foo");
    Vue.ls.get("boo", 10); // 如果没有设置boo返回默认值10

    // let callback = val => {
    //   console.log("localStorage change", val);
    // };

    //Vue.ls.on("foo", callback); //侦查改变foo键并触发回调
    //Vue.ls.off("foo", callback); //不侦查

    //Vue.ls.remove("foo"); // 移除
  }
}).$mount("#app");
