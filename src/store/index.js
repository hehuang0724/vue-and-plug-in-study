import Vue from "vue";
import Vuex from "vuex";
//这里直接引用axios调取接口，而不是使用this.$axios，因为vuex文件里this不指向Vue实例
import axios from "axios";

//使用vuex
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    count: 0,
    users: [],
  },
  // mutations用来操作state中的成员变量
  mutations: {
    increment(state) {
      state.count++;
    },
    decrement(state) {
      state.count--;
    }
  },
  // actions 异步操作，用来发请求对state中的成员进行修改
  actions: {
    // 这里的state参数需要传一个对象，否则不会改变state
    getUsers({ state }) {
      axios({
        method: "get",
        url: "http://jsonplaceholder.typicode.com/users",
      }).then(res => {
        console.log(res);
        state.users = res.data;
      });
    }
  },
  // modules 模块化状态管理
  modules: {},
  //  getters用来加工state成员给外界，类似于computed
  getters: {
    doubleCount(state) {
      return state.count * 2;
    },
  },
});
